package com.gl.MagicBooks.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.gl.MagicBooks.model.*;

import io.swagger.annotations.Api;

import com.gl.MagicBooks.dao.AdminRepository;
import com.gl.MagicBooks.dao.BookRepository;

@RestController
@Api(tags = "Admin")
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminRepository admin;
	@Autowired
	BookRepository bookDao;
	
	@GetMapping("/HomePage")
	public List<Book> home()
	{
		List<Book> booklist = bookDao.findAll();
		return booklist;
	}
	
	@PostMapping("/register")
	public String register(@RequestBody Admin user)
	{
		admin.save(user);
		return "Register Success";
	}
	
	
	@PostMapping("/login")
	public String login(@RequestBody Admin user)
	{
		if(admin.login(user.getAdminname(), user.getPassword()) != null)
		{
			return "Login Success";
		}
		else
		{
			return "Login Failed";
		}
	}
	
	@PostMapping("/InsertBook")
	public Book insertBook(@RequestBody Book book)
	{
		return bookDao.save(book);
		//return "Book Inserted Successfully";
	}
	
	@PostMapping("/UpdateBook")
	public String update(@RequestBody Book b)
	{
		System.out.println(b.getCopiessold());
		bookDao.updateBook(b.getCopiessold(),b.getPrice(), b.getBookId());
		return "SuccessFully Updated";
	}
	
	@PostMapping("/DeleteBook/{id}")
	public String deleteBook(@PathVariable int id)
	{
		bookDao.deleteById(id);
		return "Successfully Deleted";
	}
	
	@PostMapping("/getBookById/{id}")
	public Book getBookById(@PathVariable int id)
	{
		Book b = bookDao.getBookById(id);
		return b;
	}
	
	@PostMapping("/getBookByName/{bookName}")
	public List<Book> getbookbyname(@PathVariable String bookName)
	{
		List<Book> bookList = bookDao.getBookByName(bookName);
		return bookList;
	}
	
	@PostMapping("/getBookByAuthor/{authorName}")
	public List<Book> getbookbyauthor(@PathVariable String authorName)
	{
		List<Book> bookList = bookDao.getBookByAuthor(authorName);
		return bookList;
	}
	
	


}
