package com.gl.MagicBooks.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.gl.MagicBooks.model.*;

import io.swagger.annotations.Api;

import com.gl.MagicBooks.dao.*;

@RestController
@Api(tags = "User")
@RequestMapping("/User")
public class UserController {
	
	@Autowired
	UserRepository userRepo;
	@Autowired
	FavouriteBookRepository favRepo;
	@Autowired
	BookRepository bookDao;
	
	
	@PostMapping("/Register")
	public String register(@RequestBody User user)
	{
		userRepo.save(user);
		return "Registered Successfully";
	}
	
	@PostMapping("/Login")
	public String login(@RequestParam String username, @RequestParam String password)
	{
		User u = userRepo.login(username, password);
		if(u != null)
		{
			return "Login Successfull";
		}
		else
		{
			return "Login Failed";
		}
	}
	
	@PostMapping("/AddFavourite")
	public String AddFavourite(@RequestParam int bookId, @RequestParam String userID)
	{
		Book book = bookDao.getBookById(bookId);
		FavouriteBooks fav = new FavouriteBooks();
		if(userRepo.existsById(userID))
		{
		User user = userRepo.getById(userID);
		System.out.println(user);
		fav.setUser(user);
		fav.setBook(book);
		if(favRepo.BookExist(user, book) == null && book != null && user != null)
		{
			favRepo.save(fav);
			return "Book Added";
		}
		else if(user == null)
		{
			return "Not a Valid User";
		}
		else if(book == null)
		{
			return "Book is not found in booklist";
		}
		else
		{
			return "Book Already Exists";
		}
		}
		else
			return "Invalid User";
	}
	
	
	@PostMapping("/getFavouriteBooks/{mail}")
	public List<FavouriteBooks> getFavouriteBooks(@PathVariable String mail)
	{
		User user = userRepo.getById(mail);
		List<FavouriteBooks> favList = favRepo.getBooks(user);
		return favList;
	}
	
	@GetMapping("/BooksDetails")
	public List<Book> retreive()
	{
		List<Book> booklist = bookDao.retreiveBooks();
		return booklist;
	}
	
	@PostMapping("/getBookById/{id}")
	public Book getBookById(@PathVariable int id)
	{
		Book b = bookDao.getBookById(id);
		return b;
	}
	
	@PostMapping("/getBookByName/{bookName}")
	public List<Book> getbookbyname(@PathVariable String bookName)
	{
		List<Book> bookList = bookDao.getBookByName(bookName);
		return bookList;
	}
	
	@PostMapping("/getBookByAuthorName/{authorName}")
	public List<Book> getbookbyauthor(@PathVariable String authorName)
	{
		List<Book> bookList = bookDao.getBookByAuthor(authorName);
		return bookList;
	}
	
	@PostMapping("/getbookByrange")
	public List<Book> getbookByrange(@RequestParam double min, @RequestParam double max)
	{
		List<Book> bookList = bookDao.getBookByPriceRange(min, max);
	    return bookList;  	
	}
	
	@GetMapping("/getBooksByHighToLow")
	public List<Book> getPricedesc()
	{
		List<Book> bookList = bookDao.OrderByDesc();
		return bookList;	
	}
	
	@GetMapping("/getBooksByLowToHigh")
	public List<Book> getPriceAsc()
	{
		List<Book> bookList = bookDao.getBookByOrder();
		return bookList;	
	}
	
	


}
