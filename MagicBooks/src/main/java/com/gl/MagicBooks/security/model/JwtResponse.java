package com.gl.MagicBooks.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class JwtResponse {
	public JwtResponse()
	{
		
	}
	private String token;

	public String getToken() {
		return token;
	}

	public JwtResponse(String token) {
		super();
		this.token = token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
