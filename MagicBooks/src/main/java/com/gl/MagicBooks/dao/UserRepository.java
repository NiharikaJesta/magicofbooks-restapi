package com.gl.MagicBooks.dao;
import com.gl.MagicBooks.model.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
	@Query("select user from User user where user.username = ?1 and user.password = ?2")
	public User login(String username, String password);
}
