package com.gl.MagicBooks.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.MagicBooks.model.Admin;


@Repository
public interface AdminRepository extends JpaRepository<Admin, String>{
	@Query("select admin from Admin admin where adminname = ?1 and password = ?2")
	public Admin login(String username, String password);

}
