package com.gl.MagicBooks.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.MagicBooks.model.*;

@Repository
public interface FavouriteBookRepository extends JpaRepository<FavouriteBooks, Integer>{
	@Query("select fav from FavouriteBooks fav where fav.user = ?1")
	public List<FavouriteBooks> getBooks(User user);
	@Query("select fav from FavouriteBooks fav where fav.user = ?1 and fav.book = ?2")
	public FavouriteBooks BookExist(User user, Book book);

}
