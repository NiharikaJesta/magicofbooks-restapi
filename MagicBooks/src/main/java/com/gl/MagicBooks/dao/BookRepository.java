package com.gl.MagicBooks.dao;
import com.gl.MagicBooks.model.*;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer>{
	@Query("select book from Book book")
	public List<Book> retreiveBooks();
	@Query("select book from Book book where book.bookId = ?1")
	public Book getBookById(int id);
	@Transactional
	@Modifying
	@Query("update Book b set b.copiessold = ?1, b.price = ?2 where b.bookId = ?3")
	public void updateBook(int copiessold, double price,  int bookid);
	@Query("select book from Book book where book.bookName= ?1")
	public List<Book> getBookByName(String name);
	@Query("select book from Book book where book.authorName = ?1")
	public List<Book> getBookByAuthor(String author);
	@Query("select book from Book book where book.price between ?1 and ?2")
	public List<Book> getBookByPriceRange(double from, double to);
	@Query("select book from Book book order by price")
	public List<Book> getBookByOrder();
	@Query("select book from Book book order by price desc")
	public List<Book> OrderByDesc();
	
}
