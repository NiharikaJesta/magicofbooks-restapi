package com.gl.MagicBooks.model;

import javax.persistence.*;

@Entity
@Table(name = "admin")
public class Admin {
	@Id
	@Column(name = "adminmail")
	private String adminmail;
	@Column(name = "adminname")
	private String adminname;
	@Column(name = "password")
	private String password;
	public String getAdminmail() {
		return adminmail;
	}
	public void setAdminmail(String adminmail) {
		this.adminmail = adminmail;
	}
	public String getAdminname() {
		return adminname;
	}
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
