package com.gl.MagicBooks.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {
	@Id
	@Column(name = "useremail")
	private String usermail;
	public String getUsermail() {
		return usermail;
	}
	public void setUsermail(String usermail) {
		this.usermail = usermail;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "user", referencedColumnName = "useremail")
	private List<FavouriteBooks> favlist;
	public List<FavouriteBooks> getFavlist() {
		return favlist;
	}
	public void setFavlist(List<FavouriteBooks> favlist) {
		this.favlist = favlist;
	}
	
}
