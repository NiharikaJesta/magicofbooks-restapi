package com.gl.MagicBooks.model;

import javax.persistence.*;

@Entity
@Table(name = "favouritebooks")
public class FavouriteBooks {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@ManyToOne(cascade = CascadeType.ALL)
//	@Column(name = "user")
	private User user;
	
	@ManyToOne(cascade = CascadeType.ALL)
//	@Column(name = "book")
	private Book book;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}

}
