package com.gl.MagicBooks.model;

import java.util.List;

import javax.persistence.*;

// This is the Book class by using this class we can store details of the books

@Entity
@Table(name = "book")
public class Book {
	@Id
	@Column(name = "bookId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookId;
	@Column(name = "bookname")
	private String bookName;
	@Column(name = "authorname")
	private String authorName;
	@Column(name = "description")
	private String description;
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getCopiessold() {
		return copiessold;
	}
	public void setCopiessold(int copiessold) {
		this.copiessold = copiessold;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	@Column(name = "genre")
	private String genre;
	@Column(name = "copiesold")
	private int copiessold = 0;
	@Column(name = "price")
	private double price;
	public String getBookName() {
		return bookName;
	}
	
	// By using this function we can returns the author name of the book
	public String getAuthorName() {
		return authorName;
	}
	// By using this function we can returns the description of the book
	public String getDescription() {
		return description;
	}
	// By using this function we can returns the Id of the book
	public int getBookId() {
		return bookId;
	}
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "book", referencedColumnName = "bookId")
	private List<FavouriteBooks> favbooks;
	public List<FavouriteBooks> getFavbooks() {
		return favbooks;
	}
	public void setFavbooks(List<FavouriteBooks> favbooks) {
		this.favbooks = favbooks;
	}
	
	
	
	
	

}
